.PHONY: all tangled tangled-scheme exports clean check

all: exports tangled-scheme

tangled:
	emacs --batch -q -l setup-emacs-for-export.el -l do-exports.el --eval "(spritely-papers-tangle-patchup)"
	libreoffice --headless --convert-to pdf spritely-framing.odt
	libreoffice --headless --convert-to pdf spritely-core.odt
	libreoffice --headless --convert-to pdf spritely-for-users.odt

tangled-scheme: tangled
	wisp2lisp taste-of-goblins.w > taste-of-goblins.scm
	wisp2lisp goblins-blog.w > goblins-blog.scm
	wisp2lisp simple-sealers.w > simple-sealers.scm

exports:
	emacs -q -l setup-emacs-for-export.el -l do-exports.el --eval "(progn (export-all-spritely-papers) (kill-emacs))"

clean:
	rm -f taste-of-goblins.w taste-of-goblins.scm \
	      goblins-blog.w goblins-blog.scm \
	      simple-sealers.w simple-sealers.scm \
	      spritely-core.html spritely-core.odt spritely-core.pdf \
	      spritely-for-users.html spritely-for-users.odt spritely-for-users.pdf \
	      spritely-framing.html spritely-framing.odt spritely-framing.pdf

check: tangled-scheme
	GUILE_LOAD_PATH=`pwd`:${GUILE_LOAD_PATH} guile test-taste-of-goblins.scm
	GUILE_LOAD_PATH=`pwd`:${GUILE_LOAD_PATH} guile test-simple-sealers.scm
	GUILE_LOAD_PATH=`pwd`:${GUILE_LOAD_PATH} guile test-goblins-blog.scm
