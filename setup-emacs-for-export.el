(require 'font-lock)
(require 'facemenu)
(require 'xref)

(load-theme 'tango)

(setq org-src-preserve-indentation nil)
(setq org-src-fontify-natively t)
(setq make-backup-files nil)
