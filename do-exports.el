(require 'wisp-mode)
(require 'scheme)
(require 'htmlize)

(defun org-paper-set-it-up ()
  (when noninteractive
    (outline-show-all)
    (font-lock-flush)
    (font-lock-fontify-buffer)
    (font-lock-flush)))

(defun org-paper-export-to-odt (org-file)
  (save-excursion
    (set-buffer (find-file-noselect org-file))
    (org-paper-set-it-up)
    (org-odt-export-to-odt)))

(defun org-paper-export-to-html (org-file)
  (save-excursion
    (set-buffer (find-file-noselect org-file))
    (org-paper-set-it-up)
    (org-html-export-to-html)))

(defun org-paper-export-to-all-formats (org-file)
  (org-paper-export-to-odt org-file)
  (org-paper-export-to-html org-file))

(defun export-all-spritely-papers ()
  (global-font-lock-mode t)
  (mapcar 'org-paper-export-to-all-formats
          '("spritely-framing.org"
            "spritely-core.org"
            "spritely-for-users.org"
            "petnames/petnames.org"
            "petnames/implementation-of-petname-system-in-existing-chat-app.org")))

(defun keywordify-wisp-file (filename)
  (save-excursion
    (set-buffer (find-file-noselect filename t))
    (revert-buffer t t)
    (beginning-of-buffer)
    (replace-regexp "^\\( +\\)#:" "\\1. #:")
    (save-buffer)))

(defun spritely-papers-tangle-patchup ()
  (save-excursion
    (set-buffer (find-file-noselect "spritely-core.org"))
    (let ((output-files (org-babel-tangle)))
      (mapcar 'keywordify-wisp-file output-files))))

;; (begin (export-all-spritely-papers) (spritely-papers-tangle-patchup))
