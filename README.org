Spritely foundational whitepapers go here.  Maybe other stuff
eventually too.

* Foundational whitepapers

 - [[file:spritely-framing.org][Spritely: New Foundations for Networked Communities]]
 - [[file:spritely-core.org][The Heart of Spritely: Distributed Objects and Capability Security]]
 - [[file:spritely-for-users.org][Spritely for Secure Applications and Communities]]

* Building (and testing!) the papers, extracting source code

Here's how to get going fast if you already have [[https://guix.gnu.org/][Guix]] installed:

#+BEGIN_SRC sh
guix shell   # setup dev environment
make         # build the papers
make check   # run unit tests
#+END_SRC

Or even easier, all in one command:

#+BEGIN_SRC sh
guix shell -- make check
#+END_SRC

You'll see an emacs window pop up during the =make= step and then
close while things are building.  Sorry, just let it happen.  Syntax
highlighting doesn't work otherwise else (would require some hacky
workarounds).

This will also spit out some =.w= files for wisp code and =.scm= files
for scheme code in the current directory.
