#+TITLE: Foundations for Decentralization: The Layered Architecture of Spritely
#+AUTHOR: Christine Lemmer-Webber, Randy Farmer (add more here as we go)

*NOTE:* This particular draft is the initial structural outline for
what became Spritely's three foundational whitepapers and is
DEPRECATED.
See [[file:~/devel/spritely-papers/README.org][README.org]] for the papers that follow it.

* Introduction

** The problems at hand

** Kinds of decentralization

 - Decentralized convergence (blockchains, quorums, etc)
 - Decentralized cooperating agents (actors, captp, etc)

** Why object capability security?

** What is Spritely?

 - nonprofit
 - Technology building
 - Standards building
 - Education
 - open source project

** How we structure the analysis of this paper

# * User experience, at a high level
#
# TODO: Put this in its own paper

* Conceptual layers

# "layercake"

TODO: The big part is the bridge between these two pieces

 - Start with the four layers, but "empty"
 - As we go through MOAD document, begin identifying needs,
   and making slots for them
 - Now in this document, the layercake + needs is already presented

* Architectural components

... now we "flat unpack" these pieces

#+BEGIN_SRC text
  Agency       | [Native] [Web] [Mobile] [Terminal]

  AppFramework | [AppCooperation] [SpaceTimeAllocation] [Search/Discovery] [Finance] [~AppDevelopment]

  Communities  | .--------------------internal-------------------------.                          .----external---.
               | [Context] [Threads/Posts] [Gov/Moderation] [Membership]                          [Bridge (out/in)]
                             ^- includes all media                                                 ^- Search/Index(/Crosspost?)

  People       | [Identity/Auth] .------relationships--------------. [Filters] [Notifications]    [Bridge (out/in)]
                  (personas)     |[PetnamesSystem][ConsentTracking]|                               ^- SSO?

     ....            ....

  Core         | .----------- dist objects ------------------------.  .--- encrypted data ----.
               | | [Objects] [Dist.Prog] [Persistence/Upgr] [Debug] | | [immutable] [mutable] |
               |                 ^- big unpack

  TCB (~OOS)   | [OS (OOScope)] [Hardware (OOScope)] [Web (OOScope)]
#+END_SRC

** Core layer

Spritely's ambitious design requires firm footing.  Building
peer-to-peer applications on contemporary programming architecture
is a complicated endeavour which requires careful planning.  We need
layers of abstractions that make building peer-to-peer applications
to be as natural as any other programming.  Spritely's core provides
these needs.

*** Spritely Goblins: Distributed, transactional object programming

At the heart of Spritely is Goblins, its distributed, quasi-functional
object programming system.[fn:on-objects]  Goblins provides an
intuitive security model, automatic local transactions for locally
synchronous operations, easy to use and efficient asynchronous
programming for objects which can live anywhere on the network, a
networking model which abstracts away these details from distracting
the programmer, powerful distributed debugging tools, and a process
persistence and upgrade model which respects its security
fundamentals.[fn:goblins-ancestry]

At present, Goblins has two implementations, one on [[https://racket-lang.org/][Racket]] (the
initial implementation), and one on [[https://www.gnu.org/software/guile/][Guile]] (which is
newer).[fn:why-scheme] While both are going to be maintained and
interoperable with each other in terms of distributed communication,
the Guile implementation is becoming the "main" implementation on top
of which the rest of Spritely is being built.[fn:why-guile] Goblins'
ideas are fairly general though and Goblins is implemented simply as a
library on top of a host programming language, and Goblins' key ideas
could be ported to any language with sensible lexical scoping (but it
might not look as nice or be as pleasant to use or elegant).

[fn:on-objects] In recent years there has been enormous pushback
against the term "object", stemming mostly from functional programming
spaces and PTSD developed from navigating complicated Java-esque class
hierarchies.  However, the term "object" means many different things;
Jonathan Rees identified [[http://www.mumble.net/~jar/articles/oo.html][nine possible properties]] associated with
programming uses of the word "object".  For Goblins, objects most
importantly means addressable entities with encapsulated behavior.
Goblins supports "distributed objects" in that it does not
particularly matter where an object lives for asynchronous message
passing; more on this and its relationship with "actors" later.

[fn:goblins-ancestry] TODO

[fn:why-scheme] TODO

[fn:why-guile] TODO

**** A taste of Goblins

Here we will give an extremely brief taste of what programming in
Goblins is like.  The following code is adapted from the Guile version
of Goblins.[fn:wisp-syntax]

First, let us implement a friend who will greet us:

#+BEGIN_SRC wisp
  define (^greeter _bcom our-name)
    lambda (your-name)
      format #f "Hello ~a, my name is ~a!"
             . your-name our-name
#+END_SRC

The outer procedure, defined by =define=, is named =^greeter=, which
is its constructor.[fn:hard-hats]
The inner procedure, defined by the =lambda= (an "anonymous
function"), is its behavior procedure.
Both of these are most easily understood by usage, so let's try
instantiating one:

#+BEGIN_SRC wisp
  define gary
    spawn ^greeter "Gary"
#+END_SRC

As we can see, =spawn='s first argument is the constructor for the
Goblins object which will be spawned.
=spawn= first passes in a "become capability" to the constructor,
which in this case we are ignoring (hence prefixing it with an
underscore with =_bcom=, which is the conventional way to note a
variable which is not used), then gathers all remaining arguments it
was passed and hands them to the constructor.
So in our case, ="Gary"= is passed as the value of =our-name=.

The constructor returns the procedure representing its current
behavior.  In this case, that behavior is a simple anonymous
=lambda=.
We can now invoke our =gary= friend using the synchronous call-return
=$= operator:

#+BEGIN_SRC wisp
  ;; Returns:
  ;;  => "Hello Gary, my name is Alice!"
  $ gary "Alice"
#+END_SRC

The behavior of =^greeter= should now be clear.

Takes one argument when spawned, our-name, which is what the
greeter refers to itself as.

[fn:wisp-syntax] Here we are using [[https://srfi.schemers.org/srfi-119/srfi-119.html]["Wisp" syntax]], a whitespace
version of Lisp.  This has all the same structural properties as
a parenthetical representation, but we have found it is a bit
less intimidating to newcomers.

[fn:hard-hats] The =^= character is conventionally prefixed on Goblins
constructors and is called a "hard hat", referring to the kind used by
construction workers.

**** Security

Goblins' powerful security approach is rooted in the intuitions of
ordinary programming: an object only has access to that which is in
its scope, and references to other objects, data, and procedures is
only done via ordinary reference passing.  If you don't have it, you
can't use it.  This style of programming is called "object
capabilities" and its ideas appear at all layers of the Spritely
stack, but especially within Goblins.[fn:more-on-ocaps]

[fn:more-on-ocaps] TODO

**** The vat model of computation

Goblins follows what is called the "vat model" of computation.
A "vat" is simply an event loop that manages a set of
objects which are "near" to each other (and similarly, objects outside
of a vat are "far" from each other).[fn:vat-terminology]  

Objects which are "near" can perform synchronous call-return
invocations in a manner familiar to most sequential programming
languages used by most programmers today.  Aside from being
a somewhat more convenient way to program, sequential invocation
is desirable because of "cheap transactionality", which we shall
expand on more later.  In Goblins, we use the =$= operator
to perform synchronous operations.

Both "near" and "far" objects are able to invoke each other
asynchronously using asynchronous message passing (in the same style
as the "classic actor model").[fn:classic-actors]  It does not
generally matter whether or not a "far" object is running within the
same OS process or machine or one somewhere else on the
network[fn:session-severance] for most programming tasks; asynchronous
message passing works the same either way.  In Goblins, we use the
=<-= operator to perform asynchronous operations.

The sender of the message is handed back a promise to which it can
supply callbacks, listening for the promise to be fulfilled with a
value or broken (usually in case of an unexpected error).
For both programmer convenience and for networked efficiency, Goblins
supports "promise pipelining": messages can be sent to promises which
have not yet resolved, and will be forwarded to the target once the
promise resolves.[fn:full-promise-vision]

As usual in the vat model of computation, individual message sends to
a vat (event loop) are queued and then handled one "turn" at a time,
akin to the way board game players take turns around a table (which is
indeed the basis for the term "turn").

The message, addressing a specific object, is passed to the recipient
object's current behavior.  This object in turn may invoke other
"near" objects (residing within the same vat), which may themselves
invoke other near objects in a synchronous and sequential call-return
style familiar to most users of most contemporary programming
languages.  Any of these invoked objects may also change their
state/behavior (behavior changes appear purely functional in Goblins;
invocations of other actors do not), spawn new objects, invoke
ordinary expressions from the host language, or send asynchronous
messages to other objects (which are only sent if the turn completes
successfully).

While the "vat model" of computation is not new (it originates in the
[[http://erights.org/][E]] programming language and can trace some of its ideas back to E's
predecessor [[http://erights.org/history/joule/][Joule]], and has since reapppeared in systems such as
[[https://agoric.com/][Agoric's]] [[https://github.com/Agoric/agoric-sdk/tree/master/packages/SwingSet][SwingSet]] kernel), Goblins brings some novel contributions to
the table in terms of transactionality and time-travel debugging,
enhancing an already powerful distributed programming paradigm,
as we shall see.

[fn:vat-terminology] TODO

[fn:classic-actors] TODO

[fn:session-severance] TODO

[fn:full-promise-vision] TODO

**** Cheap transactionality

Special to Goblins is the transactional nature of vat turns: unhandled
errors result in a turn being rolled back automatically (or more
accurately, simply never being committed to the root transactional
heap), preventing unintended data corruption.  This cheap
transactionality means that errors in Goblins are much less eventful
and dangerous to deal with than in most asynchronous programming
languages.  Significantly less effort needs to be spent on cleanup
when time is reverted to a point where a mess never
occured.[fn:quasi-functional-heres-why]

[fn:quasi-functional-heres-why] We said earlier that Goblins was
"quasi-functional"; objects self-updates appear functional (specifying
the next procedure to represent their behavior for their next
invocation) but invoking another object appears imperative.  This
feature of cheap transactionality is a direct consequence.  However,
providing something with the functionality of Goblins usually requires
some complicated monad; Goblins hides this with an abstracted
"implicit monad", giving all of its convenience with none of its extra
manual plumbing.

#+BEGIN_QUOTE
Programming languages generally hide a monad; the real questions
are "Which monad?" and "How is it hidden?"

  -- Jonathan Rees, individual correspondence
#+END_QUOTE

**** Time-travel distributed debugging

The same transactional-heap design of Goblins can be used for other
purposes.  A distributed debugger inspired by E's [[http://www.erights.org/elang/tools/causeway/index.html][Causeway]] is planned,
complete with message-tracing mechanisms.
This will be even more powerful when combined with
already-demonstrated time travel features,[fn:terminal-phase-time-travel]
allowing programmers to debug a program in the state of an error when
it occured.

[fn:terminal-phase-time-travel] One early demonstration of this idea
was shown in the runs-in-your-terminal space shooter game
[[https://gitlab.com/dustyweb/terminal-phase][Terminal Phase]], built as a demo to show off Spritely Goblins.
The entire core game was built before even considering that time
travel would be an easy feature to add, and a
[[https://dustycloud.org/blog/goblins-time-travel-micropreview/][time travel demonstration was added]] within less than three hours
changing no core game code but merely wrapping the toplevel of the
program; its design fell out naturally from what Goblins already
provided in the way it was used.

**** Process persistence and upgrade

Much of programming traditionally involves reading and writing state
of a program to a more persistent medium, generally files on a disk or
some specialized database.  Web applications in particular spend an
enormous amount of effort moving between database representations and
runtime behavior.  A dizzying zoo of acronymns, ranging from CRUD to
ORM to MVC tend to represent the range of such systems.  These are
still complex to use, and upgrading them even moreso, which is often
treated as a separate problem entirely.

Goblins' design makes making this easy even more pressing as its
authority model is literally its runtime graph of objects, and so for
many programs, this graph will need to be persisted and later
restored.  For example, a multiplayer fantasy game might have to keep
track of many rooms, the inhabitants of those rooms including various
monsters and players, players' inventory, and many clever other
objects and mechanisms which might even be defined while the game is
running.  Ad-hoc serialization of such a system would be too hard to
keep our heads on straight about, and so we would like some way of
having our system do the serialization of our process for us.

One option would be to use an underlying language runtime
serialization system (many lisp and smalltalk systems have supported
this for decades).  However, this is wasteful; most serialized systems
can be restored from a recipe of their construction rather than their
current state at a fraction of the storage cost.

Objects which self-describe their own serialization appears to be a
solution to only saving what we need.  But we also need our system to
be secure: it would be dangerous if objects could claim to have
authority that they do not... for example, in our game, we would not
player-built objects to be able to claim or dispense in-game currency
or powers which they did not have.

Spritely Goblins provides a mechanism for all this which is
built-in.[fn:aurie-history][fn:aurie-part-of-goblins]  The serializer
starts with root objects and calls a special serializer method on each
object, asking each object for its depiction.  This serialization
mechanism is "sealed" off from normal usage; only the serializer can
unseal it, preventing objects from interrogating each other for
information or capabilities they should not have access
to.[fn:rights-amplification]  Since objects can only hand out
references they have access to, no object can claim to the serializer
to have authority it did not already have.

Since walking the entire object graph is expensive, we can take
advantage of reading turn-transaction-delta information to only
serialize objects which have changed, making Aurie performant.

The system is restored by walking the graph in reverse and applying
each self-portrait to its build recipe.  Restoring an object also
turns out to be a great time to run upgrade code and as we build out
Goblins we plan to capture many upgrade patterns into a common
library.

The serialized graph can be used for another purpose: we can use it to
create a running visualization of a stored ocap system, further
helping programmers debug systems and understand the authority graph
of a running system.

[fn:aurie-history] The ideas for our serialization/upgrade
mechanism stem from comments from comments by Jonathan A. Rees about
[[https://odontomachus.wordpress.com/2020/12/09/pickling-uneval-unapply/]["uneval" and "unapply"]] and the E programming language's [[http://erights.org/data/serial/jhu-paper/index.html][Safe
Serialization Under Mutual Supsicion]] paper (along with discussions
between Randy Farmer and Mark S. Miller while at Electric
Communities which preceded this).  

[fn:aurie-part-of-goblins]Originally we had built this system
as a separate mechanism we called "Aurie", symbolized by a character
made out of fire which was continuously extinguished and re-awakened
like a phoenix.  However we discovered that many programs, and even
many of the standard library pieces which Goblins ships with, were in
want of such a system, so Aurie's flame became folded into Goblins
itself.

[fn:rights-amplification] This is a common ocap pattern called "rights
amplification" which is beyond the scope of this paper to explain but
which is worth looking up.

**** Distributed representations: the Unum Pattern, PubSub, and CRDTs

In general when we have spoken so far in this paper of distributed
objects, we have been referring to objects with one specific
"location".  But many systems are actually more complicated than this.
For example, Alisha and Ben might both be in the same chatroom and
there may be a distinct address for Alisha and Ben's personas; if we
ask whether or not Carol means the same Alisha as Ben, she should have
no problem saying "yes, this is the same person", and this can be as
simple as address comparison.[fn:eq-isnt-simple]

The [[http://habitatchronicles.com/2019/08/the-unum-pattern/][Unum Pattern]] is a conceptual framework that encompasses the idea
of an object with many different presences.  Consider a teacup sitting
on a table in a virtual world.  Where does it live?  On the server?
What about its representation in your client?  What about the
representation on another player's client?  What about in your
mind?[fn:perceptions-and-relativity] While there is one conceptual
object of the teacup, there are likely many presences representing it.
Information and authority pertaining to the teacup may also be
asymmetric;[fn:asymmetric-authority] you might know that the teacup
has a secret note sealed inside it and I might not.[fn:thanks-chip]

One difference between the unum pattern and most other distributed
pattern literature is that the unum pattern is particularly interested
in /distributed behavior/ rather than /distributed data/.  Distributed
data may be emergent from distributed behavior, but it is only one
application.  In the unum pattern, many different presences cooperate
together performing different roles, sometimes even responding to
messages in a manner semi-opaque to each other.

The unum pattern is typically implemented via several messaging
patterns: the reply pattern, the point-to-point pattern, the neighbor
pattern, and the broadcast pattern.  Keen observers might notice that
a subset of the unum pattern, applied to data, is a publish-subscribe
(PubSub) system, which is common in social media architecture design
(ActivityPub is more or less a glorified data-centric
publish-subscribe classic actor model implementation designed for
social media on the web).
For large-scale distribution of messages, the [[http://www.erights.org/elib/distrib/unum/index.html][Ampitheater Pattern]]
will be supported.

However, in recent times there have been advancements in convergent
information architectures with research on
[[https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type][conflict-free replicated data types]].  Goblins plans on implementing
a standard library of CRDT patterns which can be thought of as a
"unum construction kit".

[fn:eq-isnt-simple] Actually, saying that this is "as simple as address
comparison" is the greatest misleading statement in this entire paper.
Object identity through address comparison, frequently referred to as
=EQ= based on the operator borrowed from lisp systems, is one of the
most complicated talks debated in the object capability security
community.  See also the [[http://www.erights.org/][erights.org]] pages on [[http://www.erights.org/elib/equality/same-object.html][Object Sameness]] and
the [[http://erights.org/elib/equality/grant-matcher/index.html][Grant Matcher Puzzle]].  These are just the tip of the iceberg of
=EQ= discussion and debate in the ocap community, and it's no surprise
why: when identity is handled /incorrectly/ it can accidentally turn
into an Access Control List or inherit their problems of ambient
authority and confused deputies.  This is part of the value of finding
patterns for these things, to help prevent users from falling into
these traps.

[fn:thanks-chip] The above explaination is modified directly from
[[http://habitatchronicles.com/2019/08/the-unum-pattern/][Chip Mornignstar's explaination of the Unum]].  Chip Morningstar
co-founded both Lucasfilms Habitat and Electric Communities (with EC
Habitat), both of which are enormous influences on Spritely's design.

[fn:asymmetric-authority] Exploiting "asymmetric authority" is the
very definition of the "confused deputy problem".  Its cause is
usually emergent from ambient authority.  Phishing attacks are an
example of confused deputy problems where the confused deputy is a
human being.  Most object capability programming does not have
confused deputy issues because to have a reference to a capability, in
the general case, means to have authority to it.  However, =EQ= and
rights amplification (which bottoms out in a kind of =EQ=) both can
re-introduce asymmetry, permitting confused deputies in careless
designs, even to ocap systems.  One might suggest removing identity
comparison altogether from such systems, and for many ocap programs
this is possible.  However a /social system/ is not very useful
without identity, so we must develop patterns that treat identity
with care.

**** Convergent machines

So far we have described Goblins as a transactional system supporting
distributed programming.  In Goblins, objects cooperate even though
the internal state and schematics of objects are mostly opaque to each
other.  This is especially true when objects are on machines disjoint
from each other over the network.

However there are times when it is useful to replicate the same
behavior on either side of the network.  If two deterministic and
equivalently configured machines/vats consume the same stream of
messages, both will have the same behavior.  The question then
becomes, who is in charge of determining the stream of messages?

With a hat tip to the folks at Agoric,[fn:machine-breakdown] we will
break up machines (or vats) into the following categories:

 - *Solo*: The standard approach in Goblins, a single machine
   running its vats independently.
 - *Quorum*: Have a limited body of members governing the
   official ordered stream of messages.
 - *Blockchain*: Open governance globally, but participation
   is throttled based on some metric such as resource consumption
   (proof of work) or data encoded on tthe chain itself (proof of
   stake).[fn:blockchain-definitions]
   
Both of these have important uses, though at the time of writing
Blockchains are being oversold for many of the cases where we believe
a solo-machine approach with Spritely Goblins with OCapN/CapTP is more
appropriate.

While Quorums have been described in recent systems as a kind of
"limited, permissioned blockchain", looking at the scaling from the
opposite direction can be helpful.  Quorums can also be useful for
simulation of shared environments.  A multiplayer game or shared
desktop can be built on top of a deterministic codebase allowing for
real time gameplay or collaboration by allowing both sides to apply
the inputs they have on their end on top of the last agreed upon
snapshot.  When inputs from the other player or user come in, the
abstracted machine can be rewound, playing the other side's inputs on
top of the previous machine state, invisibly updating the desktop or
game to accomodate the other side's choices without introducing any
lag-based stuttering.[fn:croquet-multiplayer-terminal-phase]

[fn:machine-breakdown] See Mark S. Miller's
[[https://www.youtube.com/watch?v=YXUqfgdDbr8][Programming Secure Smart Contracts]] talk and Dean Tribble's
[[https://papers.agoric.com/train-hotel-problem/][Trans, Hotels, and Async]], both of which introduce these breakdown of
conceptual

[fn:blockchain-definitions] We have chosen a more sepecific use of
"Blockchain", but that word is actually fairly underspecified and
[[https://dustycloud.org/blog/what-is-a-blockchain-really/][many people mean different things]] which has contributed to much conflict
over the term.

[fn:croquet-multiplayer-terminal-phase] This is how [[https://wiki.c2.com/?OpenCroquet][OpenCroquet]] works
and is also how most retro video game console emulators implement
multiplayer.  Imagining rewinding and invisibly replaying inputs from
a cooperative 2-player version of Terminal Phase using its
[[https://dustycloud.org/blog/goblins-time-travel-micropreview/][time travel feature]] is one way to think about how this would work
on top of Spritely Goblins.

# - Language environment that supports capabilities as object
#   references
#
#     http://mumble.net/~jar/pubs/secureos/secureos.html
#
# - Argument passing is your capability transfer abstraction...
#   the same way programmers already program
#
# - Automatic translation between remote objects and local references
#   ... you use OCapN without even thinking about OCapN
#   (including distributed gc and promise pipelining)
#
# - /Full/ vision of promises... you can program against objects
#   that don't even exist yet!  (rephrase, less smug)
#
# - Transactions: easy atomic sequential operations
#
# - /Convergent machines:/ (needs more attention)
#   - Quorum consensus: OpenCroquet-style convergent vats
#   - Global consensus: (aherm, then you can build "blockchains")
#
# - Unum: abstractly distributed object with mutliple presences
#   (different ways to do this... pubsub, CRDTs, etc)
#   Key difference: you might *NOT* have the same vision of a presence!
#
#   (More thoughts to maybe expand: And whether or not synchrony
#   matters: Light that's in your virtual room with a switch: someone
#   switches it on or off... "relativity")
#
#   - Ampitheater pattern
#
# - Time travel debugging: cheap ability to move backwards and forwards
#   between previous system states
#
#   - Debugging against the version of things that broke
#
#   - Cooperative exception disclosure with trusted machines / across
#     vats
#
#   - Causeway-like debugging tool: have a "casuality tree"
#
#   - Auto-granovetter diagram (related to persistence)

*** OCapN: A Protocol for Secure, Distributed Systems

 OCapN (the Object Capability Network)

  - CapTP: Distributed, secure networked object programming.  Provides
    familiar message passing patterns, no distinction between
    asynchronous programming against local vs remote objects.

    - Distributed garbage collection: servers can cooperate to free
      resources that are no longer needed

    - Promise pipelining: massive parallelization and network
      optimization.  provides convenience of sequential programming
      without round trips.

  - Netlayers: abstract interface for establishing secure connections
    between two parties

    - Temporal connection abstraction: live session vs store and forward

    - Peer to peer and traditional client-server support

    - Quorum...!  (Blockchains!)

  - URI structure and certificates for establishing connections to
    servers and initial object connections

*** Application and library safety

 - Language: All modules are untrusted by default.
   Loading a module doesn't mean it can do dangerous things.

 - OS: All programs are untrusted by default.
   Loading a program doesn't mean it can do dangerous things.

 - The same approach taken for ocap programming applies to
   language and OS safety: pass around capabilities to do
   things such as file operations, network access, reading
   from the keyboard, etc.
   (Solitaire example)

 - "Sandboxing" is not enough if it doesn't allow for granting
   new permissions at runtime.  You'll end up with too rigid
   of systems which means people will pass in more authority
   than they should.  (Needs better framing, or link off-site
   to something.)

 - Graphical interfaces for understanding and granting permissions

*** Portable, encrypted storage

 - Some kinds of distributed files where the contents of the
   stored files aren't known or of concern to the hosting provider

 - There is no "official" host for those files... anyone who has
   them can keep them alive

 - We want both immutable file types and mutable types


** Social layer

*** Personas, Context, Trust management

# anti-soylent-green: "Personas aren't necessarily people!!!"
#   anti-identity joke there ;)

Requirements (assuming existing ocap system):
 - Personas (Unums with user-exposed names/viewing/reference)
   - Profiles
     - General
     - Contextual
   - Notifications (big unpack)
 - Petname system
 - Credential system (mine the VC space)
 - Requirements/ToE system
 - Granted permissions tracking
 - Context management (giant unpack)
# - Relationship graphs

 - Login bridging (maybe move down)

*** Social Communities

 - Messaging "style"
   - Ephemeral (IM, slack, signal, IRC, etc)
   - Persistent (forums, blogposts, etc)

 - Relationships (social graph)
   - Types
     - Friends <->
     - Follows ->
     - ?
   - Per-persona
     - Per-context
   - Between objects (???)
   - "Communication entry paths": stamps and mutual friend, etc

 - Membership (hub and spoke)
   - Social context / "group"

   - Actions / Activities
     - Create
     - Administrate
     - Remove
     - Archive / Replicate
     - Join
     - Leave
     - Invites

 - Posts, threads, and artifacts (wiki)
   - All the AS2 stuff (Notes, Videos, Audio, etc)
   - Importantly, participants in the thread can help share content
     but participation in the thread is an essential capability, it's
     not ambient "what threads do you have, hmmmm?"

 - Content feedback and reputation

 - Moderation and governance
   - Flexible policies
     - Pre-approval
     - Post-moderation
     - Flagging and review
     - Majority vote / quorum
     - Free for all
     - Bulk/AI moderation
   - (JIT) Behavior facilitation/correction semantics
     - Poster-side pre-flagging assistance
     - Governance-enforced filtering

 - Analytics?! (privacy preserving?!)

"Do we use the same petname system for personas, apps, inventory,
currency types, webpage bookmarks, etc etc"

There are two types of links:

 - "Live object things": very behavior'y
   Sturdyrefs
 - "File-like (updateable) data things"
   (Encrypted) Portable Storage

 - URLs to HTTP stuff
   (screws up by mixing the two above things)
   (this is a bridge)

*** Discovery

Recognize the difference between these two:

 - Kinds of information
   - "fundamentally public content"
     - you expect/want this to be available to everyone
     - you can't prevent indexing of this anyway
     - social agreement SHOULD be: everyone can copy

   - "fundamentally private content"
     - readable to only those who have permission to read
     - social agreements / community guidelines of when people
       can/should copy information out
       (are these ever machine-readable, and does the system
       help comply eg MarkM's "admonishment systems"?)
     - voluntary oblivious compliance auto-expiry
       (eg "context /requests/ post removal after 30 days")

 - Finding personas
 - Finding services
 - Search
   - Internal, within your own agency: public and private
   - Public content indexing and search
 - App directory

 - Sitemap-style inclusion protocol (as opposed to robots.txt as an
   exclusion protocol) as part of bridge

*** Service bridging

 - ActivityPub
 - DID Auth, whatever

** Application layer

*** Distributed finance

 - Agoric's stuff
   - ERTP
 - Wallets blah blah

*** Peer-to-peer user agent ("The Agency")

#+BEGIN_SRC text
(agency: native, terminal, web)               (applandia)
         .---------.                :  .-----------.
         | display |<---------------:--| gc-canvas |
         '---------'                :  '-----------'
           ^                        :      in | ^ out
           |                        :         V |
   .-----------.                    :      .---------.  .--------.
   |   Navi    |--------------------:----->|         |->| #room1 |
   |coop kernel|                    :      | goblin- |<-'--------'
   |           |   .-----.          :      | chat    |  .--------.
   '-----------'<--|facet|<---------:------|         |->| #room2 |
     |^    |       '-----'          :      '---------'<-'--------'
     ||    '----------------.        ~~~~~~~~~~ | ~~~~~~~~~~~~~~~~~~
     V|                     |        .----------'
   .-------.                |        |
   | aurie |                V        V
   '-------'             .------.  .---------.
  (persistence)          | brux |->| persona |
                         '------'  '---------'
                  (id mgmt) |            |
                            |            V             .-------.
                            |         .-----------. .->| alice |
(handwave:                  '-------->|{"alice": O--'  '-------'
 kernel also                          | "bob":   O--.  .-------.
 gives access to:                     | ......}   | '->| bob   |
  - "OS" caps                         '-----------'    '-------'
  - inter-app coop)                        |
                                           V
(handwave:                            .-----------.
 permissions management gui)          | {...}     |
                                      '-----------'
#+END_SRC

 - Inter-app cooperative kernel
   - Capability grants between apps
   - Capability grants to "OS" caps
 - Permissions management gui
 - Petnames integration
 - Highly configurable display system
   - Per-app canvases
   - Petnames overlay tooling
   - Inter-app cooperation (with display)
 - Persona profile display/management
 - App persistence/storage
 - Context management interface
   - Persona selection
   - App/context selection
 - Flavors of the agency
   - Native
     - Linux (cross-binary support to OSX, Windows)
     - Mobile
   - Terminal
   - Web
 - App install (and permissions configuration)
 - Discovery integration (all kinds)
 - Invitations
 - Wallet/financial integration
 - Bridges (and can you make it clear that things get risky here?)
   - Crossposting
   - HTML rendering of documents
   - Single-sign-on/identity mapping
 - "Unum display and UI" (intentionally vague)
 - Dev tools
   - MUDdebug (MUD-ish debug tools)
   - Integrated debugger
   - Minimal code editor
   - Authority scope
     - Authority of present context
     - Top-capability authority

*** Possible demo applications

 - Basic social stuff
   - Ephemeral chat
   - Persistent, hostless message boards

 - Convergent app examples
   - opencroquet's desktop demo,
   - collaborative text editors)

 - Trading card game mockup (Sushimon)

 - SpritelyMUD

 - Graphical virtual world example (Liberated Pixel Cup)

 - Minimal (toy?) wallet

 - Introduction paths
   - Stamps
   - Friend-of-a-friend
     - Email as an introduction flow

 - Streaming videoconference

* Specifications, documentation, standardization, multiple implementations

 - Racket version
 - Guile version
 - Web version (WASM or JS)

 - Document patterns, create multiple implementations of patterns
   (and patterns as interfaces-to-implement become specifications)

* Foundations for these foundations: a trusted TCB

** Collaboration with the Guix project

* Status of work and demonstrations thereof

* Conclusion

# "What is this document for?"

# "Gosh this sounds like a lot" <- correct conclusion!
# "But it seems like you know what you're doing and have it started"
# "So I guess this is going to require a lot of resources"
